﻿using System;
using System.Threading.Tasks;
using Grpc.Net.Client;
using Otus.Teaching.PromoCodeFactory.WebHost;

namespace Otus.Teaching.PromoCodeFactory.GrpcClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new GrpcCustomer.GrpcCustomerClient(channel);

            Console.ReadLine();         
            await GetCustomerByIdAsync(client);
            Console.ReadLine();
            await GetAllCustomersAsync(client);
            Console.ReadLine();
            await CreateCustomerAsync(client);
            Console.ReadLine();
            await GetAllCustomersAsync(client);
            Console.ReadLine();
        }

        /// <summary>
        /// Gets customer by Id
        /// </summary>
        /// <returns></returns>
        static async Task GetCustomerByIdAsync(GrpcCustomer.GrpcCustomerClient client)
        {
            Console.WriteLine("============================================================");            

            var customer = await client.GetAsync(new CustomerIdGrpc()
            {
                Id = "a6c8c6b1-4349-45b0-ab31-244740aaf0f0"
            });

            Console.WriteLine("Customer: " + customer);
        }

        /// <summary>
        /// Gets all customers
        /// </summary>
        /// <returns></returns>
        static async Task GetAllCustomersAsync(GrpcCustomer.GrpcCustomerClient client)
        {
            Console.WriteLine("============================================================");            

            var customers = await client.GetAllAsync(new EmptyGrpc());

            foreach (var customer in customers.Customers)
            {
                Console.WriteLine("Customer: " + customer);
            }
        }

        /// <summary>
        /// Creates customer
        /// </summary>
        static async Task CreateCustomerAsync(GrpcCustomer.GrpcCustomerClient client)
        {
            Console.WriteLine("============================================================");            

            var customer = new CreateOrEditCustomerRequestGrpc()
            {
                Email = "test@test.com",
                FirstName = "FirstName",
                LastName = "LastName",
            };

            customer.PreferenceIds.Add("c4bda62e-fc74-4256-a956-4760b3858cbd");

            var result = await client.InsertAsync(customer);

            Console.WriteLine("Customer created");
        }
    }
}
