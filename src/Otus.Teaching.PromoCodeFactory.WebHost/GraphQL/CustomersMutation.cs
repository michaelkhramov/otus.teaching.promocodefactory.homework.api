﻿using HotChocolate;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    /// <summary>
    /// Customers Mutation for GraphQL
    /// </summary>
    public class CustomersMutation
    {
        /// <summary>
        /// Creates customer
        /// </summary>
        /// <param name="request">Request</param>
        /// <param name="customerService">Customer Service</param>
        /// <returns>Customer response</returns>        
        public async Task<Models.CustomerResponse> CreateCustomerAsync(Models.CreateOrEditCustomerRequest request, [Service] CustomerService customerService)
        {
            return await customerService.CreateCustomerAsync(request);
        }

        /// <summary>
        /// Edits customer
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <param name="request">Request</param>
        /// <param name="customerService">Customer Service</param>
        /// <returns>True of False</returns>
        public async Task<bool> EditCustomersAsync(Guid id, Models.CreateOrEditCustomerRequest request, [Service] CustomerService customerService)
        {
            return await customerService.EditCustomersAsync(id, request);
        }

        /// <summary>
        /// Deletes customer
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <param name="customerService">Customer Service</param>
        /// <returns>True of False</returns>
        public async Task<bool> DeleteCustomerAsync(Guid id, [Service] CustomerService customerService)
        {
            return await customerService.DeleteCustomerAsync(id);
        }
    }
}
