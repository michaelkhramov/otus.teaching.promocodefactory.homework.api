﻿using HotChocolate;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    /// <summary>
    /// Customers Query for GraphQL
    /// </summary>
    public class CustomersQuery
    {
        /// <summary>
        /// Gets all customers
        /// </summary>
        /// <param name="customerService">Customer Service</param>
        /// <returns>List of customers</returns>
        public async Task<List<Models.CustomerShortResponse>> CustomersAsync([Service] CustomerService customerService)
        {
            return await customerService.GetCustomersAsync();
        }

        /// <summary>
        /// Gets customer by Id
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <param name="customerService">Customer Service</param>
        /// <returns>Customer</returns>
        public async Task<Models.CustomerResponse> CustomerAsync(Guid id, [Service] CustomerService customerService)
        {
            return await customerService.GetCustomerAsync(id);
        }
    }
}
