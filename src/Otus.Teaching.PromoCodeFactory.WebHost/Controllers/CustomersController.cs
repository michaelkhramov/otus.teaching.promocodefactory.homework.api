﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController: ControllerBase
    {
        /// <summary>
        /// Customer Service
        /// </summary>
        private readonly ICustomerService _customerService;

        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="customerService">Customer Service</param>
        public CustomersController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        /// <summary>
        /// Gets all customers
        /// </summary>
        /// <returns>List of customers</returns>
        [HttpGet]
        public async Task<ActionResult<List<Models.CustomerShortResponse>>> GetCustomersAsync()
        {
            return Ok(await _customerService.GetCustomersAsync());
        }

        /// <summary>
        /// Gets customer by Id
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns>Customer</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<Models.CustomerResponse>> GetCustomerAsync(Guid id)
        {
            return Ok(await _customerService.GetCustomerAsync(id));
        }

        /// <summary>
        /// Creates customer
        /// </summary>
        /// <param name="request">Request</param>
        /// <returns>Customer response</returns>
        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(Models.CreateOrEditCustomerRequest request)
        {
            var customer = await _customerService.CreateCustomerAsync(request);
            return CreatedAtAction(nameof(GetCustomerAsync), new { id = customer.Id }, customer.Id);            
        }

        /// <summary>
        /// Edits customer
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <param name="request">Request</param>
        /// <returns>True of False</returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, Models.CreateOrEditCustomerRequest request)
        {
            var result = await _customerService.EditCustomersAsync(id, request);
                        
            if (!result)
                return NotFound();

            return NoContent();
        }

        /// <summary>
        /// Deletes customer
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns>True of False</returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            var result = await _customerService.DeleteCustomerAsync(id);

            if (!result)
                return NotFound();

            return NoContent();
        }
    }
}