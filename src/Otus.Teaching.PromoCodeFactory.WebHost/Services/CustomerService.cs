﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    /// <summary>
    /// Customer Service
    /// </summary>
    public class CustomerService: ICustomerService
    {
        /// <summary>
        /// Repositories
        /// </summary>
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="customerRepository"></param>
        /// <param name="preferenceRepository"></param>
        public CustomerService(
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository
        )
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Gets all customers
        /// </summary>
        /// <returns>List of customers</returns>
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return response;
        }
        
        /// <summary>
        /// Gets customer by Id
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns>Customer</returns>
        public async Task<CustomerResponse> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            var response = new CustomerResponse(customer);

            return response;
        }
        
        /// <summary>
        /// Creates customer
        /// </summary>
        /// <param name="request">Request</param>
        /// <returns>Customer response</returns>
        public async Task<CustomerResponse> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //Получаем предпочтения из бд и сохраняем большой объект
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);

            return new CustomerResponse() { Id = customer.Id };
        }

        /// <summary>
        /// Edits customer
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <param name="request">Request</param>
        /// <returns>True of False</returns>
        public async Task<bool> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {            
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return false;

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return true;
        }

        /// <summary>
        /// Deletes customer
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns>True of False</returns>
        public async Task<bool> DeleteCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return false;

            await _customerRepository.DeleteAsync(customer);

            return true;
        }
    }
}
