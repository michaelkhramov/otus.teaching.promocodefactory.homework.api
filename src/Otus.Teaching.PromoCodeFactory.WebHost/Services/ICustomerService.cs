﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    /// <summary>
    /// ICustomerService
    /// </summary>
    public interface ICustomerService
    {
        /// <summary>
        /// Gets all customers
        /// </summary>
        /// <returns>List of customers</returns>
        Task<List<CustomerShortResponse>> GetCustomersAsync();

        /// <summary>
        /// Gets customer by Id
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns>Customer</returns>
        Task<CustomerResponse> GetCustomerAsync(Guid id);

        /// <summary>
        /// Creates customer
        /// </summary>
        /// <param name="request">Request</param>
        /// <returns>Customer response</returns>
        Task<CustomerResponse> CreateCustomerAsync(CreateOrEditCustomerRequest request);

        /// <summary>
        /// Edits customer
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <param name="request">Request</param>
        /// <returns>True of False</returns>
        Task<bool> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request);

        /// <summary>
        /// Deletes customer
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns>True of False</returns>
        Task<bool> DeleteCustomerAsync(Guid id);
    }
}
