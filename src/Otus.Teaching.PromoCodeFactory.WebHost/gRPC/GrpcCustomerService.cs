﻿using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.gRPC
{
    /// <summary>
    /// gRPC Customer Service
    /// </summary>
    public class GrpcCustomerService: GrpcCustomer.GrpcCustomerBase
    {
        /// <summary>
        /// Customer Service
        /// </summary>
        private readonly ICustomerService _customerService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="customerService">Customer Service</param>
        public GrpcCustomerService(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        /// <summary>
        /// Gets All customers
        /// </summary>
        /// <param name="request"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task<CustomerShortResponseListGrpc> GetAll(EmptyGrpc request, ServerCallContext context)
        {
            var customers = await _customerService.GetCustomersAsync();
            var result = new CustomerShortResponseListGrpc();
            result.Customers.AddRange(customers.Select(x => new CustomerShortResponseGrpc
            {
                Email = x.Email,
                FirstName = x.FirstName,
                Id = x.Id.ToString(),
                LastName = x.LastName
            }));

            return result;
        }

        /// <summary>
        /// Get one customer
        /// </summary>
        /// <param name="request"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task<CustomerResponseGrpc> Get(CustomerIdGrpc request, ServerCallContext context)
        {
            var customer = await _customerService.GetCustomerAsync(Guid.Parse(request.Id));

            if (customer == null)
                throw new Exception("Customer not found");

            var result = new CustomerResponseGrpc()
            { 
                Email = customer.Email,
                FirstName = customer.FirstName,
                Id = customer.Id.ToString(),
                LastName = customer.LastName
            };

            if (customer.Preferences != null)
            {
                result.Preferences.AddRange(customer.Preferences.Select(x => new PreferenceResponseGrpc()
                {
                    Id = x.Id.ToString(),
                    Name = x.Name
                }));
            }

            if (customer.PromoCodes != null)
            {
                result.PromoCodes.AddRange(customer.PromoCodes.Select(x => new PromoCodeShortResponseGrpc()
                {
                    BeginDate = x.BeginDate,
                    Code = x.Code,
                    EndDate = x.EndDate,
                    Id = x.Id.ToString(),
                    PartnerName = x.PartnerName,
                    ServiceInfo = x.ServiceInfo
                }));
            }

            return result;
        }


        /// <summary>
        /// Creates customer
        /// </summary>
        /// <param name="request"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task<CustomerResponseGrpc> Insert(CreateOrEditCustomerRequestGrpc request, ServerCallContext context)
        {
            var createResult = await _customerService.CreateCustomerAsync(new Models.CreateOrEditCustomerRequest()
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                PreferenceIds = request.PreferenceIds.Select(x => Guid.Parse(x)).ToList()
            });

            if (createResult == null)
                throw new Exception("Error during creation");
            
            return new CustomerResponseGrpc() { Id = createResult.Id.ToString() };
        }

        /// <summary>
        /// Updates customer
        /// </summary>
        /// <param name="request"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task<EmptyGrpc> Update(UpdateCustomerRequestGrpc request, ServerCallContext context)
        {
            await _customerService.EditCustomersAsync(Guid.Parse(request.Id), new Models.CreateOrEditCustomerRequest()
            {
                Email = request.Customer.Email,
                FirstName = request.Customer.FirstName,
                LastName = request.Customer.LastName,
                PreferenceIds = request.Customer.PreferenceIds.Select(x => Guid.Parse(x)).ToList()
            });

            return new EmptyGrpc();
        }

        /// <summary>
        /// Deletes customer
        /// </summary>
        /// <param name="request"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task<EmptyGrpc> Delete(CustomerIdGrpc request, ServerCallContext context)
        {
            await _customerService.DeleteCustomerAsync(Guid.Parse(request.Id));
            return new EmptyGrpc();
        }
    }
}
